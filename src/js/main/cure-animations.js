var cure = angular.module('cure');

cure.animation('.cureRow', function() {
  return {
    enter: addRow,

    leave: removeRow,

    beforeAddClass: function(element, className, done) {
      if (className == 'ng-hide') {
        removeRow(element, done);
      } else {
        done();
      }
    },
    removeClass: function(element, className, done) {
      if (className == 'ng-hide') {
        addRow(element, done);
      } else {
        done();
      }
    },

    move: function(element, done) {
      var els = jQuery(element).find('td div');
      els.css('opacity', 0.5);
      els.animate({'opacity': 1}, 350, done);

      return function(isCancelled) {
        if (isCancelled) {
          els.stop();
        }
      };
    },
  };

function addRow(element, done) {
      var els = jQuery(element).find('td div');
      els.css('max-height', '0px');
      els.css('display', 'block');
      els.css('overflow', 'hidden');

      element.find('td').animate({'padding-top': '8px','padding-bottom': '8px'}, 250, 'swing', done);
      els.animate({'max-height': 51}, 250, 'swing', function(){
        els.css("overflow", "visible");
        done();
      });

      return function(isCancelled) {
        if (isCancelled) {
          els.stop();
        }
      };
    }

  function removeRow(element, done) {
      var height = jQuery(element).height();
      var els = jQuery(element).find('td div');
      els.css('max-height', height);
      els.css('overflow', 'hidden');
      els.css('display', 'block');

      element.find('td').animate({'padding-top': '0px','padding-bottom': '0px'}, 250, 'swing', done);
      els.animate({'max-height': '0px'}, 250, 'swing', function(){
        els.css("overflow", "visible");
        done();
      });

      return function(isCancelled) {
        if (isCancelled) {
          els.stop();
        }
      };
    }
});