var cure = angular.module('cure');

cure.factory('tools', function() {
    var regex = new RegExp('^\\d*([,.]\\d+)?$');

    function isAmount(value) {
        return regex.test(value);
    }

    // Converts a string containing decimals with either comma or dot, 
    // to proper amount (xxx,dd) with only two decimals (truncates).
    function convertAmount(text) {
        if (typeof text === 'string' && text.length > 0 && isAmount(text)) {
            text = text.replace(".", ",");
            var ore;

            if (text.indexOf(',') != -1) {
                text = text + "00";
                // truncate two digits after comma.
                var idx = text.indexOf(',') + 1;
                var decimals = text.substring(idx, idx + 2);
                var kr = text.substring(0, text.indexOf(','));
                ore = parseInt(decimals, 10) + (kr.length > 0 ? parseInt(kr, 10) * 100 : 0);
            } else {
                ore = parseInt(text, 10) * 100;
            }
            return oreToString(ore);
        }
    }

    function oreToString(ore) {
        if (angular.isNumber(ore)) {
            // Inject a comma after the two last digits.
            var amount = ore.toString();

            if (amount.length === 2) {
                return "0," + amount;
            } else if (amount.length === 1) {
                return "0,0" + amount;
            } else {
                return amount.substring(0, amount.length - 2) + ',' + amount.substring(amount.length - 2);
            }
        }
    }

    function parsePersonnumber(value) {
        // Remove all whitespaces...
        if (value) {
            var newValue = value.replace(/\s/g, '');
            if(/^\d{11}$/.test(newValue) ) {
                var mm = parseInt(value.substring(0, 2), 10);
                var dd = parseInt(value.substring(2, 4), 10);
                var yy = parseInt(value.substring(4, 6), 10);

                var ListofDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
                var isLeapYear = (!(yy % 4) && yy % 100) || !(yy % 400);

                if (mm == 1 || mm > 2) {
                    if (dd > ListofDays[mm - 1]) {
                        return undefined;
                    }
                } else if (mm == 2) {
                    if ((!isLeapYear && dd >= 29) || (isLeapYear && (dd > 29))) {
                        return undefined;
                    }
                }

                var calcArray1 = [3, 7, 6, 1, 8, 9, 4, 5, 2];
                var calcArray2 = [5, 4, 3, 2, 7, 6, 5, 4, 3, 2];

                var calcBase = newValue.substring(0, 9);
                calcBase = calcBase + calcControlDigit(calcBase, calcArray1);
                calcBase = calcBase + calcControlDigit(calcBase, calcArray2);

                if (calcBase === newValue) {
                    return newValue;
                }
            }
        }
        return undefined;
    }


    function parsePhonenumber(value) {
        if (value) {
            var newValue = value.replace(/\s/g, '');
            if(/^\d{8}$/.test(newValue) ) {
                return newValue;
            }
        }
        return undefined;
    }


    function calcControlDigit(value, array) {
        var ctrl = 0;
        value.split('').forEach(function(value, index) {
            ctrl += parseInt(value, 10) * array[index];
        });
        var ctrlMod11 = ctrl % 11;
        if (ctrlMod11 === 0) {
            return '0';
        } else if (ctrlMod11 === 1) {
            return '-';
        }

        return '' + (11 - ctrlMod11);
    }

    return {
        isAmount: isAmount,
        convertAmount: convertAmount,
        oreToString: oreToString,
        parsePersonnumber: parsePersonnumber,
        parsePhonenumber: parsePhonenumber
    };
});