var cure = angular.module('cure');

cure.factory('cureFormatDirective', function($timeout) {
    'use strict';

    function abstractFormatDirective(parser, formatter) {
        return {
            restrict: 'A',
            require: 'ngModel',
            scope: {
                field: '=ngModel'
            },
            
            link: new CreateLinkFunction(parser, formatter)
        };

        function CreateLinkFunction(parser, formatter) {
            var fnFormatter = formatter || function(value){ return value; };
            return function(scope, element, attrs, ctrl) {
                element.bind('blur', _format);
                element.bind('focus', _load);
                scope.$on('resetScope', _reset);
                scope.$on('setScope', _set);
                scope.$on('focusOn', _setFocus);
                
                ctrl.$parsers.unshift(function(value) {
                    scope.backup = value;
                    var newValue = parser(value);
                    ctrl.$setValidity('regexValidate', !! newValue);

                    return newValue;
                });
                ctrl.$formatters.unshift(function(value) {
                    return fnFormatter(value);
                });

                function _setFocus(event, name) {
                    if( name === element[0].name ) {
                        $timeout(function(){
                            element[0].focus();
                        });
                    }
                }

                function _reset() {
                    scope.backup = '';
                    if(!scope.field) {
                        element[0].value = '';
                    }
                }

                function _set() {
                    scope.backup = scope.field;
                }

                function _format() {
                    var backupValue = (scope && scope.backup)?scope.backup:'';
                    scope.$apply(function() {
                        element[0].value = (scope.field === undefined ? backupValue : fnFormatter(scope.field));
                    });

                }

                function _load() {
                    if (scope && scope.backup) {
                        scope.$apply(function() {
                            element[0].value = scope.backup;
                        });
                    }
                }
            };
        }
    }

    return abstractFormatDirective;
});


cure.directive('cureCapitalize', function(cureFormatDirective) {
    var parser = function(value) {
        var valid = value && value.length >= 2;

        // Capitalize first character in each word.
        return valid ? value.split(' ').map(function(val) {
            return val.substring(0, 1).toUpperCase() + val.substring(1).toLowerCase();
        }).join(' ') : undefined;
    };
    var newDirective = cureFormatDirective(parser, parser);
    return newDirective;
});

cure.directive('cureAmount', function(cureFormatDirective, tools) {
    var parser = function(value) {
        var valid = tools.isAmount(value);
        return valid ? tools.convertAmount(value) : undefined;
    };
    var format = function(value) {
        return value ? tools.convertAmount(value) : '';
    };

    var newDirective = cureFormatDirective(parser, format);
    return newDirective;
});

cure.directive('curePersonnumber', function(cureFormatDirective, tools) {
    var parsePnr = function(value) {
        return tools.parsePersonnumber(value);
    };

    var formatPnr  = function(value) {
        return value ? value.substring(0,6) + ' ' + value.substring(6) : '';
    };

    var newDirective = cureFormatDirective(parsePnr, formatPnr);
    return newDirective;
});

cure.directive('curePhone', function(cureFormatDirective, tools) {
    var parse = function(value) {
        return tools.parsePhonenumber(value);
    };

    var format = function(value) {
        return value ? value.substring(0,3) + ' ' + value.substring(3,5) + ' ' + value.substring(5,8) : '';
    };

    var newDirective = cureFormatDirective(parse, format);
    return newDirective;
});
cure.directive('cureEmail', function(cureFormatDirective, tools) {
    var parse = function(value) {
        return value ? value.toLowerCase() : value;
    };

    var newDirective = cureFormatDirective(parse);
    return newDirective;
});