var cure = angular.module("cure");

cure.factory('employeeService', function() {
	    var employees = [{
        "id": 1,
        "firstname": "Ravinder",
        "lastname": "singh",
        "hourlyWage": "100,00",
        "externalId": "05097829499",
        "email": "road@dumpster.no",
        "phone": "92832232"
    }, {
        "id": 2,
        "firstname": "Nosey",
        "lastname": "Picks",
        "hourlyWage": "102.0",
        "externalId": "15048926816",
        "email": "flicks@dumpster.no",
        "phone": "87121311"
    }];

	function getEmployees() {
		return employees;
	}

	function addEmployee(employee) {
        employee.id = employees.length;
        employees.push(employee);
	}

	function removeEmployee(employee) {
        var index = employees.indexOf(employee);
        if (index > -1) {
            employees.splice(index, 1);
        }
	}

	return {
		getEmployees: getEmployees,
		addEmployee: addEmployee,
		removeEmployee: removeEmployee
	};
	
});