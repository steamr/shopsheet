var cure = angular.module('cure');

cure.filter('capitalize', function() {
    return function(value) {
        return value.split(' ').map(function(val) {
            return val.substring(0, 1).toUpperCase() + val.substring(1).toLowerCase();
        }).join(' ');
    };
});

cure.filter('amount', function(tools) {
    return function(value) {
        return tools.convertAmount(value);
    };
});

cure.filter('personnumber', function() {
    return function(value) {
        return value.substring(0,6) + ' ' + value.substring(6);
    };
});

cure.filter('phone', function() {
    return function(value) {
        return value ? value.substring(0,3) + ' ' + value.substring(3,5) + ' ' + value.substring(5,8) : '';
    };
});

var monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
cure.filter('monthNameFull', function() {
    return function(value) {
        return monthNames[value];
    };
});

var monthNamesShort = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
cure.filter('monthNameShort', function() {
    return function(value) {
        return monthNamesShort[value];
    };
});

cure.filter('prefixZeros', function() {
    return function(value, digits) {
        angular.isNumber(value)
    };
});
