// Dependencies
var cure = angular.module('cure', ['ui.bootstrap', 'ngAnimate', 'ngRoute']);

cure.config(function($routeProvider) {
    $routeProvider
    .when('/timesheet.html', {
            templateUrl : 'timesheet.html',
            controller: 'timesheetController'
    }).when('/employee.html', {
            templateUrl : 'employee.html',
            controller: 'employeeController'
    }).otherwise({
        redirectTo: '/employee.html'
    });
});

 cure.controller('RouteController', function($scope, $route, $routeParams, $location) {
     $scope.$route = $route;
     $scope.$location = $location;
     $scope.$routeParams = $routeParams;
 });


 cure.controller('employeeController', function($scope, $rootScope, employeeService) {
    $scope.employees = employeeService.getEmployees();

    $scope.sortField = 'id';
    $scope.sortReverse = false;

    $scope.setSort = function(field) {
        if ($scope.sortField === field) {
            $scope.sortReverse = !$scope.sortReverse;
        } else {
            $scope.sortReverse = false;
            $scope.sortField = field;
        }
    };

    $scope.createEmployee = function() {
        $scope.$broadcast('createEmployee');
    };
});

/* Separate controller for reseting the input forms (only for newEmployee). */
cure.controller('newForm', function($scope, employeeService) {
    var parentScope = $scope.$parent;
    $scope.newEmployee = null;

    $scope.$on('createEmployee', function() {
        if ($scope.newEmployee === null) {
            $scope.newEmployee = {};
            $scope.$broadcast('focusOn', 'firstname');
        }
    });

    $scope.cancelNewEmployee = function() {
        $scope.newEmployee = null;
        $scope.$broadcast('resetScope');
        $scope.form.$setPristine();
    };

    $scope.persistNewEmployee = function() {
        employeeService.addEmployee($scope.newEmployee);
        $scope.newEmployee = null;
        $scope.$broadcast('resetScope');
        $scope.form.$setPristine();
    };
});


cure.controller('editForm', function($scope, employeeService) {
    var parentScope = $scope.$parent;

    $scope.editEmployee = function(employee) {
        if (!employee.edit) {
            employee.original = angular.copy(employee);
            employee.edit = true;
            $scope.$broadcast('setScope');
            $scope.$broadcast('focusOn', 'firstname');
        }
    };

    $scope.cancelEditEmployee = function(employee) {
        if (employee.edit) {
            employee.edit = false;
            var original = employee.original;
            angular.copy(original, employee);
            $scope.$broadcast('resetScope');
            $scope.form.$setPristine();
        }
    };

    $scope.saveEmployee = function(employee) {
        if (employee.edit) {
            employee.edit = false;
            $scope.$broadcast('resetScope');
            $scope.form.$setPristine();
        }
    };

    $scope.deleteEmployee = function(employee) {
        employeeService.removeEmployee(employee);

        if(parentScope.employees.length === 0 ) {
            parentScope.createEmployee();
        }
    };
});


cure.controller('timesheetController', function($scope, employeeService) {
    $scope.employees = employeeService.getEmployees();
    $scope.periods = [];
    $scope.selectedPeriod = {};

    init();

    function init() {
        // get current month
        var now = new Date();
        var firstInPreviousMonth = new Date(now.getFullYear(), now.getMonth()-1, 1, 0, 0, 0, 0);
        var month = firstInPreviousMonth.getMonth();
        var year  = firstInPreviousMonth.getFullYear();

        $scope.selectedPeriod.month = month;
        $scope.selectedPeriod.year = year;

        for (var i = 0; i < 4; i++) {
            var period = {
                month: (month+i) % 12,
                year:  year + ((month+i)>11?1:0)
            };
            $scope.periods.push(period);
        }
    }

    $scope.getDaysInMonth = function(period) {
        var month = period.month;
        var year  = period.year;
        var aDay = new Date(year, month, 1, 0, 0, 0, 0);
        var days = [];

        do {
            var day = aDay.getDate();
            days.push(day);
            aDay.setDate(day+1);
        } while(month === aDay.getMonth());

        return days;
    };

    $scope.changePeriod = function(period) {
        $scope.selectedPeriod = period;
    }
});